﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissapearAfterTime : MonoBehaviour
{
    public int timeToVanish = 4;
    void OnEnable()
    {
        Invoke("Dissapear", timeToVanish);
    }

    void Dissapear()
    {
        gameObject.SetActive(false);
    }
}
