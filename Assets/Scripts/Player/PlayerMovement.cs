﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 2.5f;
    SpriteRenderer spriteRenderer;
    public GameObject player;
    Animator animator;
    bool canMove = true,    
         isOverNav = false,
         goingUp = false;

    public Transform cameraPosition;

    Transform auxPosition, 
              auxCameraPosition;

    int screenIndex;

    void Start()
    {
        spriteRenderer = player.GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (canMove)
        {
            float movH = Input.GetAxis("Horizontal");
            transform.Translate(new Vector3(movH * speed * Time.deltaTime, 0, 0));
            animator.SetFloat("vSpeed", Mathf.Abs(movH));

            if (movH > 0.1f)
            {
                transform.localScale = new Vector3(1,1,1);
                //spriteRenderer.flipX = false;
            } 
            else if (movH < -0.1f)
            {
                transform.localScale = new Vector3(-1,1,1);
                //spriteRenderer.flipX = true;
            }
        }
        else
        {
            animator.SetFloat("vSpeed", 0);
        }

        if (isOverNav)
        {
            if (Input.GetAxis("Vertical") > 0.3f)
            {
                //Debug.Log("Triggered");
                animator.SetBool("GoingBack", true);
                SetCanMove(false);
                if(!goingUp)
                {
                    Invoke("MoveUp", 0.3f);
                    goingUp = true; 
                }
                Invoke("SetGoingBackFalse", 0.32f);
            }
        }
    }

    void MoveUp()
    {
        transform.position = auxPosition.position;
        cameraPosition.position = auxCameraPosition.position;
    }

    void SetGoingBackFalse()
    {
        goingUp = false;
        animator.SetBool("GoingBack", false);
        SetCanMove(true);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Screen"))
        {
            transform.position = other.gameObject.GetComponent<SceneChanger>().GetNewPosition().position;
            cameraPosition.position = other.gameObject.GetComponent<SceneChanger>().GetNewCameraPosition().position;
            EventManager.playerMoved(transform.position.x, transform.position.y);
        }

        if (other.CompareTag("Nav"))
        {
            isOverNav = true;
            auxPosition = other.gameObject.GetComponent<SceneChanger>().GetNewPosition();
            auxCameraPosition =  other.gameObject.GetComponent<SceneChanger>().GetNewCameraPosition();
            //SetCanMove(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
         if (other.CompareTag("Nav"))
        {
            isOverNav = false;
        }
    }

    public void SetCanMove(bool value){canMove = value;}

}
