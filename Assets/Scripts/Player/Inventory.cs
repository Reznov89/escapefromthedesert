﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    //REWORK
    bool slot1Status = false,
         slot2Status = false;

    public Image imgSlot1, 
                 imgSlot2;

    public Sprite hand;

    [SerializeField]
    GameObject item1,
               item2;

    public bool GetSlot1() => slot1Status;
    public bool GetSlot2() => slot2Status;

    void Start()
    {
        UpdateHUD();
    }

    public void AddNewItem(GameObject newItem, int slot)
    {
        if (newItem == null) return;

        if (slot == 1)
        {
            slot1Status = true;
            item1 = newItem;
        }
        else
        {
            slot2Status = true;
            item2 = newItem;
        }

        UpdateHUD();
    }

    public void SetEmptySlot(int slot)
    {
        if (slot == 1)
        {
            slot1Status = false;
            item1 = null;
        }
        else
        {
            slot2Status = false;
            item2 = null;
        }
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        if (slot1Status == true)
        {
            imgSlot1.sprite = item1.GetComponent<ItemProperties>().GetItemSprite();
        }
        else
        {
            imgSlot1.sprite = hand;
        }

        if (slot2Status == true)
        {
            imgSlot2.sprite = item2.GetComponent<ItemProperties>().GetItemSprite();
        }
        else
        {
            imgSlot2.sprite = hand;
        }
    }

    public GameObject GetObject(int slot)
    {
        return slot == 1 ? item1 : item2;
    }

    public int GetItemID(int slot)
    {
        int reply = -1;
        if (slot == 1 && slot1Status)
        {
            reply = item1.GetComponent<ItemProperties>().GetItemID();
        }
        else if (slot == 2 && slot2Status)
        {
            reply = item2.GetComponent<ItemProperties>().GetItemID();
        }

        return reply;
    }

}
