﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{   
    bool isOverObject = false;

    public Inventory inventory;
    PuzzleSolving puzzles;
    //public AllObjects objects;
    //Items itemToPick = null;
    GameObject objectPicked,
                item1, 
                item2;

    void Awake()
    {
        puzzles = GetComponent<PuzzleSolving>();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Pickable"))
        {
            isOverObject = false;
            objectPicked = null;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Pickable"))
        {
            //Debug.Log("Item");
            isOverObject = true;
            objectPicked = other.gameObject;
        }
    }

    void Update()
    {
        if (isOverObject)
        {
            if (Input.GetButtonDown("Fire1") || Input.GetAxis("Fire1") > 0.5f)
            {
                CheckItems(1);
            }

            if (Input.GetButtonDown("Fire2") || Input.GetAxis("Fire2") > 0.5f)
            {
                CheckItems(2);        
            }
        }

        
    }

    void CheckItems(int n)
    {
        if (n == 1)
        {
            if (!inventory.GetSlot1()) //si es false indica que esta vacio y se puede juntar
            {
                PickObject(objectPicked, 1);
            }
            else
            {
                ReplaceItem(objectPicked, 1);
            }
        }
        else
        {
            if (!inventory.GetSlot2())
            {
                PickObject(objectPicked, 2);
            }
            else
            {
                ReplaceItem(objectPicked, 2);
            }
        }
    }

    void PickObject(GameObject item, int slot)
    {
        //condicion para que no pueda juntar la caja cuando sobre ella
        if (puzzles.GetIfIsOnBox()) return;

        inventory.AddNewItem(item, slot);
        if (slot == 1) 
        {
            item1 = item;
            item.transform.parent = transform;
            item.SetActive(false);
        }
        else
        {
            item2 = item;
            item.transform.parent = transform;
            item.SetActive(false);
        }
    }

    void ReplaceItem(GameObject item, int slot)
    {
        GameObject dummy = inventory.GetObject(slot);
        inventory.AddNewItem(item, slot);
        DropItem(slot);
    }


    public void DropItem(int slot)
    {
        if (slot == 1)
        {
            item1.transform.localPosition = new Vector3(0,0,0);
            inventory.SetEmptySlot(1);
            item1.transform.parent = null;
            item1.SetActive(true);
        }
        else
        {
            item2.transform.localPosition = new Vector3(0,0,0);
            inventory.SetEmptySlot(2);
            item2.transform.parent = null;
            item2.SetActive(true);
        }
    }

    public void UseAndDiscard(int slot)
    {
        if (slot == 1)
        {
            item1 = null;
            inventory.SetEmptySlot(1);
        }
        else
        {
            item2 = null;
            inventory.SetEmptySlot(2);
        }
    }


    /*void CarryItemOnBack(GameObject item)
    {
        item.transform.parent = transform;
        item.transform.position = transform.position;
        item.transform.localRotation = Quaternion.Euler(0,0,220);
        item.transform.localPosition = new Vector3(-0.3f,1f,0);
    }*/

}
