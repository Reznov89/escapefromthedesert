﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSolving : MonoBehaviour
{
    bool isOverBox = false,
         onTheBox = false,
         isOverHideout = false, 
         hide = false,
         onVent = false,
         onDoor = false;
    Transform currentPlayerPosition,
              otherObject;
    public PlayerMovement movement;
    public PlayerInteraction playerInteraction;
    public GameObject playerSprite;
    //public Player playerStats;
    public Inventory inventory;
    GameObject puzzleToInteract;

    Door thisDoor;

    Collider2D puzzleCollider;

    int itemId = -1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Pickable"))
        {
            if (other.gameObject.GetComponent<ItemProperties>().GetItemID() == ItemsReference.BOX && other.gameObject.activeSelf)
            {
                isOverBox = true;
            }
            otherObject = other.gameObject.transform;
        }

        if (other.CompareTag("Hideout"))
        {
            isOverHideout = true;
        }

         if (other.CompareTag("Vent"))
        {
            onVent = true;
            puzzleToInteract = other.gameObject;
        }

        if (other.CompareTag("Door"))
        {
            onDoor = true;
            thisDoor = other.GetComponent<Door>();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //otherObject = null;
        if (other.CompareTag("Pickable"))
        {
            puzzleToInteract = null;
        }

        if (other.CompareTag("Hideout"))
        {
            isOverHideout = false;
        }

        if (other.CompareTag("Vent"))
        {
            onVent = false;
            puzzleToInteract = null;
        }

        
        if (other.CompareTag("Door"))
        {
            onDoor = false;
            thisDoor = null;
        }
    }

    void Update()
    {
        //aca van a ir las acciones posibles con el botón acción
        if (Input.GetButtonDown("Fire3"))
        {
            UpToTheBox();
            Hide();
            CheckDoor();
        }

        //L1 | LB | K
        if (Input.GetButtonDown("Use1"))
        {
            //Debug.Log("Pressed k");
            CheckInventory(1);
        }

        // R1 | RB | L
        if (Input.GetButtonDown("Use2"))
        {
             //Debug.Log("Pressed l");
            CheckInventory(2);
        }
    }

    void UpToTheBox()
    {
        if (isOverBox)
        {
            if (!onTheBox)
            {
                transform.position = new Vector3(otherObject.position.x, transform.localPosition.y + 1.2f, 0);
                movement.SetCanMove(false);
                onTheBox = true;
                Debug.Log("Sobre la caja");
            }
            else
            {
                transform.position = new Vector3(otherObject.position.x, transform.localPosition.y - 1.2f, 0);
                onTheBox = false;
                movement.SetCanMove(true);
                Debug.Log("Se bajo de la caja");
            }
        }
    }

    void Hide()
    {
        if (isOverHideout)
        {
            if (!hide)
            {
                //playerStats.isHide = true;
                hide = true;
                movement.SetCanMove(false);
                playerSprite.SetActive(false);
            }
            else
            {
                //playerStats.isHide = false;
                hide = false;
                movement.SetCanMove(true);
                playerSprite.SetActive(true);
            }
        }
    }
    //Esta funcion va a chequear uno por uno los items posibles para usar
    void CheckInventory(int slot)
    {
        itemId = -1;
        itemId = inventory.GetItemID(slot);

        if (CheckBoxItem(itemId, slot)) playerInteraction.DropItem(slot);
        
        if (puzzleToInteract != null)
        {
            if (CheckVent(itemId)) playerInteraction.UseAndDiscard(slot);
        }
    }

    bool CheckVent(int itemId)
    {
        if (itemId == ItemsReference.COIN && onVent)
        {
            puzzleToInteract.GetComponent<Vent>().Trigger();
            puzzleCollider = puzzleToInteract.GetComponent<Collider2D>();
            puzzleCollider.enabled = false;
            return true;
        }else return false;
    }

    bool CheckBoxItem(int itemId, int slot)
    {
        return itemId == ItemsReference.BOX ? true : false;   
    }

    void CheckDoor()
    {
        if(!onDoor) return;

        if(!thisDoor.IsOpen())
        {
            int doorId = thisDoor.GetDoorID();

            //Llamo esta funcion para que haga el sonido de que esta cerrada
            thisDoor.Open();
        }
        else
        {
        }

    }

    public bool GetIfIsOnBox() => onTheBox;


    
}
