﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool isOpen;
    public int doorId;
    public AudioSource audioSource;
    public AudioClip closed, 
                     open;

    SceneChanger sceneChanger;
    Collider2D doorCollider;
    
    void Awake()
    {
        //lo vamos a pasar a un iniciador
        Init();
    }

    public void Init()
    {
        audioSource = GetComponent<AudioSource>();
        sceneChanger = GetComponentInChildren<SceneChanger>();
        doorCollider = GetComponent<Collider2D>();
    }
    
    public void Open()
    {
        if (isOpen)
        {
            doorCollider.enabled = false;
            audioSource.PlayOneShot(open, .8f);
        }
        else
        {
            audioSource.PlayOneShot(closed, .8f);
        }
    }


    // Update is called once per frame
    public void Unlock()
    {
        isOpen = true;
        sceneChanger.SetCanBeUsed(true);
        doorCollider.enabled = false;
    }

    public bool IsOpen() => isOpen;
    public int GetDoorID() => doorId;
}
