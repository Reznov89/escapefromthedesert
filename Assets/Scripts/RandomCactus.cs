﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCactus : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public List<Sprite> spriteList;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        int index = Random.Range(0,spriteList.Count);
        spriteRenderer.sprite = spriteList[index];
        int side = Random.Range(0,2);
        if (side == 0) spriteRenderer.flipX = true; else spriteRenderer.flipX = false;
    }

  
}
