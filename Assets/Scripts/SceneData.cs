﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneData : MonoBehaviour
{
    public int id;

    public GameObject graphicsToCull;
    
    public int GetSceneId() => id;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            EventManager.changeCamera(id);
            //Debug.Log(id);
        }
    }

    public void CullElements()
    {
        graphicsToCull.SetActive(false);
    }

    public void ShowElements()
    {
        graphicsToCull.SetActive(true);
    }
}
