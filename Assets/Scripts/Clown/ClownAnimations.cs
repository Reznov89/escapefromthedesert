﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownAnimations : MonoBehaviour
{
    Animator animator;
    public void Init()
    {
        animator = GetComponent<Animator>();
    }

    public void PerformAttack()
    {
        animator.SetTrigger("Attack");
    }

    public void WalkAround()
    {
        animator.SetBool("isMoving", true);
    }
}
