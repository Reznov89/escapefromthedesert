﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownMovement : MonoBehaviour
{
    public float clownSpeed = 3.5f; 
    bool moveRight = true,
         isMoving = true;

    public ClownAnimations animations;
    
    public void Init()
    {
        animations.Init();
    }
    public void SetMovement(bool shouldMove)
    {
        isMoving = shouldMove;  
    }

    // Update is called once per frame
    void Update()
    {
        if(!isMoving) return;

        if(moveRight)
        {
            transform.Translate(1 * Time.deltaTime * clownSpeed,0,0);
            animations.WalkAround();
        }
        else
        {
            transform.Translate(-1 * Time.deltaTime * clownSpeed,0,0);
            animations.WalkAround();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Screen"))
        {
            transform.position = other.gameObject.GetComponent<SceneChanger>().GetNewPosition().position;
        }

        if (other.CompareTag("Player"))
        {
            SetMovement(false);
            animations.PerformAttack();
            Invoke("BackToChase",1f);
        }
    }

    void BackToChase()
    {
        SetMovement(true);
    }

    public void SetNewDirection(bool moveToRight)
    {
        moveRight = moveToRight;
    }
}
