﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingPlant : MonoBehaviour
{
    public bool goingLeft;
    public Transform plant;

    void OnEnable()
    {
        Invoke("DestroyMe", 3f);
    }
    
    // Update is called once per frame
    void Update()
    {
       if (goingLeft)
       {
           plant.Rotate(Vector3.forward * Time.deltaTime * 200);
           transform.Translate(Vector3.left * Time.deltaTime * 5);
       } 
       else
       {
            plant.Rotate(Vector3.back * Time.deltaTime * 200);
            transform.Translate(Vector3.right * Time.deltaTime * 5);
       }
    }

    void DestroyMe()
    {
        gameObject.SetActive(false);
    }
}
