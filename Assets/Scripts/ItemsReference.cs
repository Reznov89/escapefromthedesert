﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ItemsReference 
{
    public static int COIN = 1;
    public static int BOX = 2;
    public static int PLIERS = 3;
    public static int KNIFE = 4;
    public static int KEY = 5;
    public static int CANISTER = 6;
    public static int CARKEY = 7;
    public static int CROWBAR = 8;
}
