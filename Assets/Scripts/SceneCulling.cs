﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneCulling : MonoBehaviour
{
    public List<SceneData> scenes;
    
    void Start()
    {
        EventManager.changeCamera += CullScenes;
    }

    // Update is called once per frame
    void CullScenes(int idToNotCull)
    {
        foreach (SceneData scene in scenes)
        {
            if (scene.GetSceneId() != idToNotCull)
            {
                scene.CullElements();
            }
            else 
            {
                scene.ShowElements();
            }
        }
    }
}
