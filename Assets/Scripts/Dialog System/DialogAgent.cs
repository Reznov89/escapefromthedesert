﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAgent : MonoBehaviour
{
    public string dialog;
    bool showDialog = true;
    // Start is called before the first frame update
    public string GetDialog()
    {
        if (showDialog)
        {
            showDialog = false;
            return dialog;
        }
        else return string.Empty;
    }
}
