﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dialogs : MonoBehaviour
{
    public Text UIdialog;
    public GameObject dialogPanel;
    float inScreenTime = 5f;
    bool isShowingDialog = false;
    string dialogToShow;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Dialog"))
        {
            dialogToShow = other.GetComponent<DialogAgent>().GetDialog();
            if (dialogToShow != string.Empty)
            {
                UIdialog.text = dialogToShow;
                dialogPanel.SetActive(true);
                isShowingDialog = true;
            }
        }
    }

    void Update()
    {
        if (isShowingDialog)
        {
            inScreenTime -= Time.deltaTime;
            if (inScreenTime < 0)
            {
                inScreenTime = 5f;
                isShowingDialog = false;
                dialogPanel.SetActive(false);
            }
        }
    }
}
