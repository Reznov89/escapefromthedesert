﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vent : MonoBehaviour
{
    Animator animator;
    bool isOpen = false;
    public SceneChanger sceneChanger;
    public AudioSource audioSource;
    
    void Start()
    {
        animator = GetComponent<Animator>();    
        audioSource = GetComponent<AudioSource>();
    }

    public void Trigger()
    {
        if (!isOpen)
        {
            animator.SetTrigger("OpenVent");
            sceneChanger.SetCanBeUsed(true);
            isOpen = true;
            audioSource.Play();
        }
    }
}
