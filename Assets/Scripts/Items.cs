﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item")]
public class Items : ScriptableObject
{
    public Sprite sprite;
    public GameObject item;
    public bool canBePicked;
    public int id;
    
}
