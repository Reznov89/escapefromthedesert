﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public Transform newPosition,
                     cameraNewPosition;

    public bool canBeUsed = true;

    // Update is called once per frame
    public Transform GetNewPosition()
    {
        return newPosition;
    }

    public Transform GetNewCameraPosition()
    {
        return cameraNewPosition;
    }

    public void SetCanBeUsed(bool value)
    {
        canBeUsed = value;
    }

    public bool GetCanBeUsed()
    {
        return canBeUsed;
    }
}
