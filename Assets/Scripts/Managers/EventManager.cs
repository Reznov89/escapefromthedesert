﻿using System;

public class EventManager 
{
    public static Action<int> changeCamera;
    public static Action<float,float> playerMoved;
    public static Action<int> doorUnlock;
}
