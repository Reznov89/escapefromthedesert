﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsManager : MonoBehaviour
{
    public List<Door> doors;

    void Awake()
    {
        //modularizar
        InitComponents();
    }

    public void InitComponents()
    {
        EventManager.doorUnlock += UnlockThisDoor;
        foreach(Door d in doors)
        {
            d.Init();
        }
    }

    //esto lo vamos a usar para desbloquear una puerta de ambos lados mas que nada
    void UnlockThisDoor(int id)
    {
        for (int i = 0; i < doors.Count; i++)
        {
            if (doors[i].GetDoorID() == id)
            {
                doors[i].Unlock();
            }
        }
    }

    void UnlockAllDoors()
    {
        foreach(Door d in doors)
        {
            d.Unlock();
        }
    }
}
