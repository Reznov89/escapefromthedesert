﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemProperties : MonoBehaviour
{
    public int id;
    public Sprite itemSprite;
    public bool canBePicked;

    public void SetAvailableToPick()
    {
        canBePicked = true;
    }
    
    public int GetItemID() => id;
    public Sprite GetItemSprite() => itemSprite;
    public bool GetCanBePicked() => canBePicked;
   
}
